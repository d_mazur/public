﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace softheme_task
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(Primes.CountPrimesTo(1000000));
            Console.ReadKey();
        }
    }

    class Primes
    {
        static public int CountPrimesTo(int n)
        {
            // sieve of Eratosthenes
            int count = 4;  // 2, 3, 5, 7 - primes

            for (int i = 5; i < n; i++)
            {
                if ((i % 2 > 0) && (i % 3 > 0) && (i % 5 > 0) && (i % 7 > 0))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
