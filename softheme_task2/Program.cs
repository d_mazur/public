﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace softheme_task2
{
    class Program
    {
        /// <summary>
        /// | curr
        /// | firt | second
        /// </summary>
        /// <param name="index"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        static int Compare(ref int index, int first, int second)    
        {                                                           
            if (first > second)                                     
              return first;

            index++;
            return second;
        }

        /// <summary>
        ///     x       x
        ///     x  |  curr  |   x
        /// |first | second | third |   x       
        /// </summary>
        /// <param name="index"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="third"></param>
        /// <returns></returns>
        static int Compare(ref int index, int first, int second, int third)      
        {                                                                      
            if (first > second && first > third)
            {
                index--;
                return first;
            }
            if (second > first && second > third)
            {
                return second;
            }
            if (third > first && third > second)
            {
                index++;
                return third;
            }

            return 0;
        }

        /// <summary>
        /// Get delta from file.
        /// The Numbers in the row are separated by spaces.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static List<int[]> GetDelta(string path)
        {
            string[] tmp;
            
            tmp = File.ReadAllLines(path);
            List<int[]> delta = new List<int[]>();

            for (int i = 0; i < tmp.Length; i++)
            {
                delta.Add(Array.ConvertAll<string, int>(tmp[i].Split(' '), Int32.Parse));
            }
            return delta;
        }

        static void Main()
        {
          
            int sum = 0;
            int index = 0;
            List<int[]> delta = GetDelta(@"d:\edu\file.txt");

            foreach (var item in delta)
            {
                if (item.Length == 1)
                {
                    sum += item[index];
                }
                else if ((index == 0) || (index == item.Length - 1))
                {
                    sum += Compare(ref index, item[index], item[index + 1]);
                }
                else if (index > 0 && index < (item.Length - 1))
                {
                    sum += Compare(ref index, item[index - 1], item[index], item[index + 1]);
                }
            }

            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
